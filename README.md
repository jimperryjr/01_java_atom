A Tour of Monsters (AToM). 
This game is designed to be run on the console (no GUI here). One player game where the player will be able to navigate through different rooms. Each room can contain at most one Monster. When the player enters a room,
they can choose to fight the monster, in which case health credits will be deducted from both the player
and the monster. The game ends when either the user runs out of health credits or simply chooses to
quit.